#include "Toolbox.h"
#include <vector>
#include <string>
#include <iostream>

using namespace std;

vector<string> split(const string & str, char splitter){
	if (str == "") return vector<string>(0);
	vector<int> semicolons;
	vector<string> tokens;
	for (size_t i = 0; i<str.length(); i++){
		if (str[i] == splitter){
			semicolons.push_back(i);
		}
	}
	for (size_t i = 0; i<semicolons.size() + 1; i++){
		if (i == 0)
			tokens.push_back(str.substr(0, semicolons[i]));
		else if (i == semicolons.size()){
			tokens.push_back(str.substr(semicolons[i - 1] + 1));
		}
		else
			tokens.push_back(str.substr(semicolons[i - 1] + 1, semicolons[i] - semicolons[i - 1] - 1));
	}
	return tokens;
}

vector<string> removeNull(const std::vector<std::string> & str){
	vector<string> res;
	for (unsigned int i(0); i < str.size(); i++)
		if (str[i] != "")
			res.push_back(str[i]);
	return res;
}