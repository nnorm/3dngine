#pragma once
#include "Common.h"

namespace Assets {
	class Texture;
}

namespace Render {

	class Material
	{

		private:

			Assets::Texture * _albedo;
			Assets::Texture * _specular;

		public:
			Material(const std::string &, const std::string &);
			Material(const Material&);
			Material& operator=(const Material&);
			~Material();
			const Assets::Texture& getAlbedo() const;
			const Assets::Texture& getSpecular() const;
			void toString(std::ostream &)const;
	};

	std::ostream & operator <<(std::ostream &, const Material &);

}