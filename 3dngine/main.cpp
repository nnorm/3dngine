#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif

#include "Common.h"
#include "Mesh.h"
#include "ObjLoader.h"
#include "Material.h"
#include "Object.h"
#include "Entity.h"
#include "transform.h"
#include "Camera.h"
#include <cmath>

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"OpenGL32.lib")
#pragma comment(lib,"glfw3.lib")


/************************************************************************/
/*                          Fen�tre de base                             */
/************************************************************************/


int main(int argc, char** args)
{
	GLFWwindow* window;


	/* Initialize the library */
	if (!glfwInit())
		return -1;
	
	/* Create a windowed mode window and its OpenGL context */
	glfwWindowHint(GLFW_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_VERSION_MINOR, 1);
	window = glfwCreateWindow(640, 480, "Hello World", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		return -1;
	}


	/* Make the window's context current */
	glfwMakeContextCurrent(window);

	glewExperimental = true;
	GLenum glewErrorHandler = glewInit();
	if (glewErrorHandler != GLEW_OK)
		std::cout << "[GLEW] Error : " << glewGetErrorString(glewErrorHandler) << std::endl;

	Assets::ObjLoader * o = Assets::ObjLoader::getInstance();
	std::vector<Assets::Mesh*> m = o->load("cube.obj");

	Render::Material mat("lenafull.png", "lenafull.png");

	/*Render::Object obj("cube", m, mat);
	obj.setPos(glm::vec4(2, 3, 4, 1));
	obj.move(glm::vec4(4, 2, 3, 1));
	obj.rotate(Render::Entity::axes::X, 90);
	obj.rotate(Render::Entity::axes::Y, 90);
	obj.setRotation(Render::Entity::axes::Z, 90);
	std::cout << obj << std::endl;*/

	/*Render::Camera cameraDeTheo("etTheoALaCamPourUnTutoMakeUp", glm::vec4(1.0f, 2.0f, 12.0f, 1.0f), glm::vec3(90, 182, 270), 45, 1920, 1080);
	std::cout << cameraDeTheo << std::endl;*/

	Render::Transform t("fuuuuussssssssssssionnnn", glm::vec4(10, 20, 30, 1.0f), glm::vec3(90, 0, 0));
	std::cout << t << std::endl;

	/* Loop until the user closes the window */
	while (!glfwWindowShouldClose(window))
	{
		/* Render here */

		/* Swap front and back buffers */
		glfwSwapBuffers(window);

		/* Poll for and process events */
		glfwPollEvents();
	}

	glfwTerminate();
	return 0;

}