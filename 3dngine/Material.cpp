#include "Material.h"
#include "PngLoader.h"
#include "Texture.h"

namespace Render {

	using namespace std;
	using namespace Assets;

	Material::Material(const string & albedo, const string & specular)
	{
		PngLoader* p = PngLoader::getInstance();
		Image* ia = p->load(albedo);
		_albedo = new Texture(ia->getData(), ia->getWidth(), ia->getHeight());
		Image* ib = p->load(specular);
		_specular = new Texture(ib->getData(), ib->getWidth(), ib->getHeight());
	}


	Material::~Material()
	{
		delete _albedo, _specular;
	}

	Material::Material(const Material& m){
		_albedo = new Texture(*m._albedo);
		_specular = new Texture(*m._specular);
	}

	Material& Material::operator = (const Material& m){
		if (this != &m){
			delete _albedo;
			delete _specular;

			_albedo = new Texture(*m._albedo);
			_specular = new Texture(*m._specular);
		}
		return *this;
	}

	const Assets::Texture& Material::getAlbedo() const{
		return *_albedo;
	}

	const Assets::Texture& Material::getSpecular() const{
		return *_specular;
	}

	void Material::toString(std::ostream &o) const{
		o << "albedo : " << _albedo->getID() << " specular : " << _specular->getID();
	}

	ostream & operator <<(ostream & o, const Material & m){
		m.toString(o);
		return o;
	}

}