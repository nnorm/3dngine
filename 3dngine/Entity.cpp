#include "Entity.h"
#include <glm/gtc/matrix_transform.hpp>

using namespace std;

namespace Render
{

	int Entity::count = 0;

	Entity::Entity(const std::string & name, const glm::vec4 & pos, const glm::vec3 & rot)
		: _name(name), _id(++Entity::count), _racine(NULL), _children(NULL)
	{
		this->setPos(pos);
		this->rotate(Entity::axes::X, rot[0]);
		this->rotate(Entity::axes::Y, rot[1]);
		this->rotate(Entity::axes::Z, rot[2]);
		Entity::refresh(this);
	}

	Entity::Entity(const Entity &){}

	Entity::~Entity()
	{
		--Entity::count;
		for (int i = 0; i < this->_children.size(); i++)
		{
			delete this->_children[i];
		}
	}

	void Entity::toString(ostream & o) const{
		o << " id : " << this->getId();
		o << "\n\tNom       : " << this->getName();
		o << "\n\tPosition  : [" << this->_pos[0] << ", " << this->_pos[1] << ", " << this->_pos[2] << ", " << this->_pos[3] << "]";
		o << "\n\tRotation  : [" << this->_rot[0] << ", " << this->_rot[1] << ", " << this->_rot[2] << "]";
		o << "\n\tMatrice: [ ";
		for (int ligne = 0; ligne < 4; ligne++) {
			o << "\n\t           ";
			for (int colonne = 0; colonne < 4; colonne++)
				o << this->_child2Parent[ligne][colonne] << " ";
		}
		o << "\n\t\t  ]";
	}

	//operators
	Entity & Entity::operator = (Entity & e)
	{
		return e;
	}

	//Accesseurs
	std::string Entity::getName() const
	{
		return this->_name;
	}

	int Entity::getId() const{
		return this->_id;
	}

	glm::mat4 Entity::getChild2Parent() const
	{
		return this->_child2Parent;
	}

	glm::mat4 Entity::getLocal2Global() const
	{
		return this->_local2Global;
	}

	Entity * Entity::getRacine() const
	{
		return this->_racine;
	}

	const vector<Entity *> & Entity::getChildren() const
	{
		return this->_children;
	}

	glm::vec4 Entity::getPos() const
	{
		return this->_pos;
	}
	
	//modificateurs
	void Entity::setChild2Parent(const glm::mat4 & child2Parent) 
	{
		this->_child2Parent = child2Parent;
		Entity::refresh(this);
		/*this->_local2Global = this->_racine->_local2Global * this->_child2Parent;

		for (Entity* e : this->_children) {

			e->setChild2Parent(e->_child2Parent);

		}*/

	}

	void Entity::setRacine(Entity * racine)
	{
		this->_racine->removeChild(this->_id);
		racine->addChild(this);
		Entity::refresh(this);
	}
	void Entity::setChildren(vector<Entity *> vecChild)
	{
		for (int i = 0; i < vecChild.size(); ++i)
			this->addChild(vecChild[i]);
		Entity::refresh(this);

		/*for (int i = 0; i < vecChild.size(); i++) {

			vecChild[i]->_local2Global = this->_local2Global * vecChild[i]->_child2Parent;

		}*/

	}

	ostream & operator <<(ostream & o, const Entity & e)
	{
		e.toString(o);
		return o;
	}

	void Entity::setPos(const glm::vec4 & pos)
	{
		this->_pos = pos;
		for (int i(0); i < 3;++i )
			this->_child2Parent[3][i] = pos[i];
		Entity::refresh(this);
	}
	
	void Entity::move(const glm::vec4 & add) {

		this->_pos += add;
		this->_child2Parent = glm::translate(this->_child2Parent, glm::vec3(add[0], add[1], add[2]));
		Entity::refresh(this);
	}

	glm::vec3 Entity::getRot() const {
		return _rot;
	}

	void Entity::setRotation(const axes & axe, float angle)
	{
		switch (axe){
		case axes::X:
			_rot[0] = angle;
			angle = glm::radians(angle);
			_child2Parent[1][1] = cos(angle);
			_child2Parent[2][2] = cos(angle);
			_child2Parent[1][2] = sin(angle);
			_child2Parent[2][1] = -sin(angle);
			break;
		case axes::Y:
			_rot[1] = angle;
			angle = glm::radians(angle);
			_child2Parent[0][0] = cos(angle);
			_child2Parent[2][2] = cos(angle);
			_child2Parent[2][0] = sin(angle);
			_child2Parent[0][2] = -sin(angle);
			break;
		case axes::Z:
			_rot[2] = angle;
			angle = glm::radians(angle);
			_child2Parent[0][0] = cos(angle);
			_child2Parent[1][1] = cos(angle);
			_child2Parent[0][1] = sin(angle);
			_child2Parent[1][0] = -sin(angle);
			break;

		}
	}

	void Entity::rotate(const axes & axe, float angle)
	{
		glm::vec3 tmp;
		switch (axe){
		case axes::X :
			_rot[0] += angle;
			tmp = glm::vec3(1, 0, 0);
			break;
		case axes::Y :
			_rot[1] += angle;
			tmp = glm::vec3(0, 1, 0);
			break;
		case axes::Z :
			_rot[2] += angle;
			tmp = glm::vec3(0, 0, 1);
			break;

		}
		angle = glm::radians(angle);
		_child2Parent = glm::rotate(_child2Parent, angle, tmp);
	}

	//m�thodes pour le tableau de children
	void Entity::addChild(Entity * e)
	{
		this->_children.push_back(e);
		//e->_local2Global = this->_local2Global * e->_child2Parent;
		e->_racine = this;
		Entity::refresh(e);
	}

	Entity * Entity::removeChild(int i)
	{
		Entity * e = nullptr;
		int toRemove = -1;

		for (int idx = 0; idx < this->_children.size() && toRemove == -1; idx++) 
			if (this->_children[idx]->getId() == i)
				toRemove = idx;
		
		if (toRemove != -1) {

			e = this->_children[toRemove];
			this->_children.erase(this->_children.begin() + toRemove);
			e->_racine = NULL;
			e->_local2Global = e->_child2Parent;
			Entity::refresh(e);

		}

		return e;
	}

	void Entity::refresh(Entity* pe) {

		if (pe->_racine != NULL)
			pe->_local2Global = pe->_racine->_local2Global * pe->_child2Parent;

		for (Entity* e : pe->_children) {

			Entity::refresh(e);

		}

	}

}