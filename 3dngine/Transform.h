#pragma once
#include "Entity.h"

namespace Render {

	class Transform : public Entity {

		public:

			Transform(const std::string&, const glm::vec4&, const glm::vec3 &);
			void toString(std::ostream & o) const;

	};

}
