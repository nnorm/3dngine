#pragma once
#include <string>
#include <GL/glew.h>

namespace Assets{
	class Texture
	{
	private:
		GLuint _textureID;
		int _width;
		int _height;

	public:
		Texture(const unsigned char*, long, long);
		~Texture();

		const GLuint& getID() const;
	};
}