#pragma once
#include "Entity.h"
namespace Render
{
	class Camera : public Entity
	{
		public:

			Camera(const std::string &, const glm::vec4 &, const glm::vec3 &, float, float, float) throw(std::logic_error);
			virtual void toString(std::ostream & o) const;

			//accesseurs
			float getFov() const;

			//modifications
			void setFov(float, float, float);

		private:
			//field of view
			float _fov;

			//matrice de percpective
			glm::mat4 _view;
	};
}

