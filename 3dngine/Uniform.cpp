#include "Uniform.h"

GL::Uniform::Uniform(const std::string & name, GLuint program)
	:name_(name), location_(glGetUniformLocation(program, name_.c_str()))
{}

GL::Uniform::~Uniform(){}

GLuint GL::Uniform::getLocation() const
{
	return location_;
}

std::string GL::Uniform::getName() const
{
	return name_;
}