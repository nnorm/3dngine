#include "Framebuffer.h"

GL::Framebuffer::Framebuffer()
	: toggled_(false)
{
	glGenFramebuffers(1, &id_);
}

GL::Framebuffer::~Framebuffer()
{
	glDeleteFramebuffers(1, &id_);
}

void GL::Framebuffer::Toggle()
{
	if (!toggled_)
		glBindFramebuffer(GL_FRAMEBUFFER, id_);
	else
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

bool GL::Framebuffer::IsToogled() const
{
	return toggled_;
}

void GL::Framebuffer::AttachTarget(GLuint target)
{
	if (!toggled_)
		Toggle();
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + targets_.size(), target, 0);
	Toggle();
}

void GL::Framebuffer::AttachDepthTarget(GLuint target)
{
	if (!toggled_)
		Toggle();
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, target, 0);
	Toggle();
}

GLuint GL::Framebuffer::GetTarget(int i) const
{
	return targets_[i];
}

GLuint GL::Framebuffer::GetDepthTarget()
{
	return depthTarget_;
}