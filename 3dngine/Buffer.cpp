#include "Buffer.h"

GL::Buffer::Buffer(GL::bufferType type)
: type_(type)
{
	switch (type)
	{
	case GL::bufferType::VAO:
		glGenVertexArrays(1, &id_);
		break;
	case GL::bufferType::VBO:
		glGenBuffers(1, &id_);
		break;
	case GL::bufferType::EBO:
		glGenBuffers(1, &id_);
		break;
	case FBO:
		glGenFramebuffers(1, &id_);
		break;
	case TFB:
		glGenBuffers(1, &id_);
		break;
	default:
		glGenBuffers(1, &id_);
		break;
	}
}
GL::bufferType GL::Buffer::type() const
{
	return type_;
}

unsigned int GL::Buffer::ID() const
{
	return id_;
}

void GL::Buffer::bind()
{
	switch (type_)
	{
	case VAO:
		glBindVertexArray(id_);
		break;
	case VBO:
		glBindBuffer(GL_ARRAY_BUFFER, id_);
		break;
	case FBO:
		glBindBuffer(GL_FRAMEBUFFER, id_);
		break;
	case EBO:
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, id_);
		break;
	case TFB:
		glBindTransformFeedback(GL_TRANSFORM_FEEDBACK_BUFFER, id_);
		break;
	default:
		glBindBuffer(GL_ARRAY_BUFFER, id_);
		break;
	}
}
void GL::Buffer::unbind()
{
	switch (type_)
	{
	case VAO:
		glBindVertexArray(0);
		break;
	case VBO:
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		break;
	case FBO:
		glBindBuffer(GL_FRAMEBUFFER, 0);
		break;
	case EBO:
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		break;
	case TFB:
		glBindTransformFeedback(GL_TRANSFORM_FEEDBACK_BUFFER, 0);
		break;
	default:
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		break;
	}
}

GL::Buffer::~Buffer(){}
