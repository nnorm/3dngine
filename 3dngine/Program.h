#pragma once
#include "Common.h"

namespace GL {
	class Shader;
	class Uniform;
	class Program
	{
	public:
		Program();
		void Attach(Shader* shader);
		void Detach(Shader* shader);
		void DetachAll();
		bool Link();
		void toggle();
		bool isActive() const;
		void AddUniform(Uniform*);
		void DeleteUniform(Uniform*);
		void DeleteAllUniforms();
		GLuint ID() const;
		~Program();

	private:
		GLuint id_;
		bool toggled_;
		std::vector<Shader*> shaders_;
		std::vector<Uniform*> uniforms_;
	};
}