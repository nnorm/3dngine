#include "PngLoader.h"
#include "picoPNG.h"

#include <fstream>
#include <vector>
#include <stdexcept>
#include <ios>

using namespace std;

namespace Assets{
	
	Image::Image(long width, long height, const std::vector<unsigned char> & data) 
		: _width(width), _height(height)
	{
		_out = new unsigned char[_width * _height*4];
		for (unsigned i(0); i < data.size(); ++i)
			_out[i] = data[i];
	}

	Image::~Image(){
		delete[] _out;
	}

	long Image::getWidth() const{ return _width; }

	long Image::getHeight() const{ return _height; }

	unsigned char * Image::getData() const{
		return _out;
	}

	void Image::toString(ostream & o)const{
		o << "width : " << _width << " height : " << _height << "\t" ;
	}

	ostream & operator <<(ostream & o, const Image & i){
		i.toString(o);
		return o;
	}

	PngLoader* PngLoader::_instance = new PngLoader();
	
    PngLoader * PngLoader::getInstance(){
		return _instance;
	}

	PngLoader::~PngLoader(){
		for (map<string, Image*>::iterator it = _cache.begin(); it != _cache.end(); ++it)
			delete it->second;
	}

	Image* PngLoader::load(const string & filePath) throw (invalid_argument){
		for (auto const &ent1 : _cache)
		if (ent1.first == filePath)
			return ent1.second;

		std::vector<unsigned char> in;

		//Lecture
		readFileToBuffer(filePath, in);

		//Decodage
		unsigned long w, h;
		vector<unsigned char> o;
        if (0 != decodePNG(o, w, h, &(in[0]), in.size()))
			throw invalid_argument("echec du decodage");
		Image* i = new Image(w, h, o);
		_cache.insert(pair<string, Image *>(filePath, i));
		cache();
		return i;
	}

	void PngLoader::cache(){
		if (_cache.size() >= Loader::CACHE){
			map<string, Image*>::iterator it = _cache.begin();
			delete it->second;
			_cache.erase(_cache.begin());
		}		
	}

	bool PngLoader::readFileToBuffer(std::string filePath, vector<unsigned char>& buffer) throw (invalid_argument) {
        ifstream file(filePath.c_str(), ios::binary ); // filepath seul ne marchait pas, test avec c.str()
		if (file.fail()) {
			throw invalid_argument("filepath incorrect");
			return false;
		}
		file.seekg(0, std::ios::end);

		int fileSize = file.tellg();
		file.seekg(0, std::ios::beg);

		fileSize -= file.tellg();

		buffer.resize(fileSize);
		file.read((char *)&(buffer[0]), fileSize);
		file.close();

		return true;
	}


	void PngLoader::clean(){
		for (map<string, Image*>::iterator it = _cache.begin(); it != _cache.end(); ++it)
			delete it->second;
		_cache.clear();
	}

}
