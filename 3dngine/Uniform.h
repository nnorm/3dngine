#pragma once
#include "Common.h"

namespace GL {
	class Uniform
	{
	public:
		void operator = (bool t)		{ glUniform1i(location_, t ? 1 : 0); };
		void operator = (int t)			{ glUniform1i(location_, t); };
		void operator = (float t)		{ glUniform1f(location_, t); };
		void operator = (const glm::vec2& t) { glUniform2f(location_, t.x, t.y); };
		void operator = (const glm::vec3& t) { glUniform3f(location_, t.x, t.y, t.z); };
		void operator = (const glm::vec4& t) { glUniform4f(location_, t.x, t.y, t.z, t.w); };
		void operator = (const glm::mat4& t) { glUniformMatrix4fv(location_, 1, false, glm::value_ptr(t)); };

		Uniform(const std::string &, GLuint);
		~Uniform();

		GLuint getLocation() const;
		std::string getName() const;
	private:
		GLuint location_;
		std::string name_;
	};
}