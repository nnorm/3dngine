#include "Transform.h"

namespace Render {

	Transform::Transform(const std::string& name, const glm::vec4& pos, const glm::vec3 & rot) : Entity(name, pos, rot) 
	{}

	void Transform::toString(std::ostream & o) const {
		o << "Transformation";
		Entity::toString(o);
	}
}
