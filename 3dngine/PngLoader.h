#pragma once
#include "Loader.h"
#include <string>

namespace Assets{

	class Image{

		private:
			unsigned char* _out;
			unsigned long _width, _height;

		public:

			Image(long, long, const std::vector<unsigned char> &);
			~Image();
			long getWidth() const;
			long getHeight() const;
			unsigned char * getData() const;
			void toString(std::ostream &)const;
	};

	std::ostream & operator <<(std::ostream & o, const Image & i);

    class PngLoader : public Loader{
		
		private:

			std::map<std::string, Image *> _cache;
			static PngLoader * _instance;
			PngLoader(){}
			bool readFileToBuffer(std::string, std::vector<unsigned char>&) throw (std::invalid_argument);
			void cache();
			~PngLoader();
			PngLoader(const PngLoader &){}
			PngLoader& operator=(const PngLoader &){}
		
		public:

			Image* load(const std::string &) throw (std::invalid_argument);
			static PngLoader * getInstance();
			virtual void clean();
	};

}
