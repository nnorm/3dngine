#include "Program.h"
#include "Shader.h"
#include "Uniform.h"

GL::Program::Program()
	: id_(glCreateProgram()), toggled_(false)
{}

void GL::Program::Attach(Shader* shader)
{
	shaders_.push_back(shader);
	glAttachShader(id_, shader->ID());
}

void GL::Program::Detach(Shader* shader)
{
	auto p = std::find(shaders_.begin(), shaders_.end(), shader);
	if (p != shaders_.end())
	{
		glDetachShader(id_, shader->ID());
		shaders_.erase(p);
	}
}

void GL::Program::DetachAll()
{
	std::vector<Shader*> shaderList = shaders_;
	for (Shader* s : shaderList)
		Detach(s);
}

bool GL::Program::Link()
{
	glLinkProgram(id_);

	int success = GL_FALSE;
	glGetProgramiv(id_, GL_LINK_STATUS, &success);

	if (success == GL_TRUE)
	{
		DetachAll();
		return true;
	}

	GLint length = 0;
	glGetProgramiv(id_, GL_INFO_LOG_LENGTH, &length);

	std::vector<GLchar> infoLog(length);
	glGetProgramInfoLog(id_, length, &length, &infoLog[0]);

#ifdef _DEBUG
	std::cout << "[Shader Program] info log : " << &infoLog[0] << std::endl;
#endif 

	return false;
}

void GL::Program::toggle()
{
	if (!toggled_)
	{
		glUseProgram(id_);
		toggled_ = true;
	}
	else
	{
		glUseProgram(0);
		toggled_ = false;
	}		
}

bool GL::Program::isActive() const
{
	return toggled_;
}

void GL::Program::AddUniform(Uniform* uniform)
{
	uniforms_.push_back(uniform);
#ifdef _DEBUG
	if (std::find(uniforms_.begin(), uniforms_.end(), uniform) != uniforms_.end())
		std::cout << "[GL] Uniform successfully added : " << uniform->getName().c_str() << std::endl;
#endif
}

void GL::Program::DeleteUniform(Uniform* uniform)
{
	if (std::find(uniforms_.begin(), uniforms_.end(), uniform) != uniforms_.end())
		uniforms_.erase(std::find(uniforms_.begin(), uniforms_.end(), uniform));
}

void GL::Program::DeleteAllUniforms()
{
	uniforms_.clear();
}

GLuint GL::Program::ID() const
{
	return id_;
}

GL::Program::~Program()
{
	glDeleteProgram(id_);
}