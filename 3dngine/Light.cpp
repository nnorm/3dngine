#include "Light.h"

using namespace std;
/*
namespace Render
{
	Light::Light(const string & name, const glm::vec4 & pos, const glm::vec4 & color, float intensity, Light::Type type) throw(logic_error)
		: Entity(name, pos), _color(color), _type(type)
	{
		if (intensity > 1.0 && intensity < 0.0)
			throw logic_error("Wrong intensity, it should be between 0.0 and 1.0");
		this->_intensity = intensity;
	}

	void Light::toString(ostream & o) const
	{
		o << "Light ";
		Entity::toString(o);
		o << "\n\tIntensity : " << this->_intensity; 
		o << "\n\tColor     : [" << this->_color[0] << ", " << this->_color[1] << ", " << this->_color[2] << ", " << this->_color[3] << "]";
		o << "\n\tPosition  : [" << this->_pos[0] << ", " << this->_pos[1] << ", " << this->_pos[2] << ", " << this->_pos[3] << "]";
	}

	//accesseurs
	Light::Type Light::getType() const
	{
		return this->_type;
	}

	float Light::getIntensity() const
	{
		return this->_intensity;
	}

	glm::vec4 Light::getColor() const
	{
		return this->_color;
	}

	//modificateurs
	void Light::setType(Light::Type type)
	{
		this->_type = type;
	}

	void Light::setIntensity(float intensity) throw(logic_error)
	{
		if (intensity > 1.0 && intensity < 0.0)
			throw logic_error("Wrong intensity, it should be between 0.0 and 1.0");
		this->_intensity = intensity;
	}

	void Light::setColor(const glm::vec4 & color)
	{
		this->_color = color;
	}
}*/