#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif

#include "Camera.h"
#include "Common.h"
#include <cmath>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>

using namespace std;

namespace Render
{
	Camera::Camera(const string & name, const glm::vec4 & pos, const glm::vec3 & rot, float fov, float width, float height) throw(logic_error) : Entity(name, pos, rot){
		this->setFov(fov, width, height);
	}


	void Camera::toString(std::ostream & o) const{
		o << "Camera";
		Entity::toString(o);
		o << "\n\tField Of View\t: " << this->_fov;
		o << "\n\tMatrice de Percpective: [ ";
		for (int ligne = 0; ligne < 4; ligne++) {
			o << "\n\t           ";
			for (int colonne = 0; colonne < 4; colonne++)
				o << this->_view[ligne][colonne] << " ";
		}
		o << "\n\t\t  ]";
	}

	//accesseurs
	float Camera::getFov() const
	{
		return this->_fov;
	}
	
	//modificateurs
	void Camera::setFov(float fov, float width, float height)
	{
		this->_fov = fov;
		float radFov = glm::radians(fov);
		this->_view = glm::perspective(cos(radFov / 2) / sin(radFov / 2), width / height, 0.1f, 100.f);
	}
}
