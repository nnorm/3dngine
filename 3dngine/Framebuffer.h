#pragma once
#include "Common.h"

namespace GL {
	class Framebuffer
	{
	public:
		Framebuffer();
		~Framebuffer();
		void Toggle();
		bool IsToogled() const;
		void AttachTarget(GLuint target);
		void AttachDepthTarget(GLuint target);
		GLuint GetTarget(int) const;
		GLuint GetDepthTarget();

	private:
		GLuint id_;
		std::vector<GLuint> targets_;
		GLuint depthTarget_;
		bool toggled_;
	};
}