#pragma once
#include "Common.h"
#include "Entity.h"
#include "Material.h"

namespace Assets{
	class Mesh;
}

namespace Render
{

	class Object : public Entity
	{
	public:
		Object(const std::string &, const std::vector<Assets::Mesh*> &, const Material &, const glm::vec4 & = glm::vec4(), const glm::vec3 & = glm::vec3());

		virtual void toString(std::ostream & o) const;

		const std::vector<Assets::Mesh *>& getMeshes() const;
		const Material& getMaterial() const;

	private:
		Material _material;
		std::vector<Assets::Mesh *> _mesh;
	};
}