#pragma once
#include "Common.h"

namespace GL {
	class Shader
	{
	public:
		Shader(GLenum);
		GLenum getType() const;
		GLuint ID() const;
		void addSource(const std::string&);
		void addSourceFromFile(const std::string&);
		bool compile();
		~Shader();

	private:
		GLuint id_;
		GLenum type_;
		std::vector<std::string> sources_;
	};
}