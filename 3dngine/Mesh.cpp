#include "Mesh.h"
#include "ObjLoader.h"
#include "Buffer.h"

namespace Assets{

	using namespace std;

	void Mesh::createBuffer(const std::vector<float *> & v, const std::vector<float *> & vt, const std::vector<float *> & vn, const std::vector<int *> & elements){
		_data = new vector<GLfloat>();
		for (int* elem : elements){
			for (int i(0); i < ObjLoader::ELEMENT_NUMBER*ObjLoader::ELEMENT_SIZE; ++i){
				int temp = elem[i];
				switch (i % ObjLoader::ELEMENT_SIZE){
				case 0:
					if (temp < 0) temp += v.size()+1;
					for (int j(0); j < ObjLoader::VSIZE; ++j)
						_data->push_back(GLfloat(v[temp - 1][j]));
					break;
				case 1:
					if (temp < 0) temp += vt.size()+1;
					for (int j(0); j < ObjLoader::TSIZE; ++j)
						_data->push_back(GLfloat(vt[temp - 1][j]));
					break;
				case 2:
					if (temp < 0) temp += vn.size()+1;
					for (int j(0); j < ObjLoader::VNSIZE; ++j)
						_data->push_back(GLfloat(vn[temp - 1][j]));
					break;
				}
			}
		}

		_vbo->bind();
		glBufferData(GL_ARRAY_BUFFER, _data->size() * sizeof(GLfloat), &((*_data)[0]), GL_STATIC_DRAW);
		_vbo->unbind();
	}

	Mesh::Mesh(const Mesh & m) : _name(m._name), _size(m._size), _vbo(new GL::Buffer(GL::bufferType::VBO)) {

		_data = new vector<GLfloat>();

		for (GLfloat gf : *(m._data))
			_data->push_back(gf);

		_vbo->bind();
		glBufferData(GL_ARRAY_BUFFER, _data->size() * sizeof(GLfloat), &((*_data)[0]), GL_STATIC_DRAW);
		_vbo->unbind();
	}

	Mesh& Mesh::operator=(const Mesh & m) {
		if (this != &m) {
			delete _data;
			delete _vbo;

			_data = new vector<GLfloat>();

			for (GLfloat gf : *(m._data))
				_data->push_back(gf);

			_vbo->bind();
			glBufferData(GL_ARRAY_BUFFER, _data->size() * sizeof(GLfloat), &((*_data)[0]), GL_STATIC_DRAW);
			_vbo->unbind();
		}
		return *this;
	}

	Mesh::~Mesh(){
		delete _data;
		delete _vbo;
	}
	
	Mesh::Mesh(const Object & o)
		: _vbo(new GL::Buffer(GL::bufferType::VBO))
	{
		_name = o._name;
		createBuffer(o._v, o._vt, o._vn, o._elements);
	}

	const GL::Buffer & Mesh::getVBO() const{
		return *_vbo;
	}

	void Mesh::toString(std::ostream & o)const{
		o << "Mesh : " << _name;
		o<< "\n\t";
		for (int i(0); i < _data->size(); i+=8)
		{
			o << "\n\t\t{ (";
			for (int j(0); j < 3; ++j)
				o << (*_data)[i+j] << ", ";
			o << ") , (";
			for (int j(3); j < 5; ++j)
				o << (*_data)[i + j] << ", ";
			o << ") , (";
			for (int j(5); j < 8; ++j)
				o << (*_data)[i + j] << ", ";
			o << ") }";
		}
	}

	std::ostream & operator <<(std::ostream & o, const Mesh & m){
		m.toString(o);
		return o;
	}
}