#include "Shader.h"

GL::Shader::Shader(GLenum type)
	: type_(type),id_(glCreateShader(type_))
{}

GLenum GL::Shader::getType() const
{
	return type_;
}

GLuint GL::Shader::ID() const 
{
	return id_;
}

void GL::Shader::addSource(const std::string& src)
{
	sources_.push_back(src);
}

void GL::Shader::addSourceFromFile(const std::string& path)
{
	std::ifstream file(path.c_str());

	if (!file)
	{
		std::cout << "[System] Could not load shader source file :" << path.c_str() << std::endl;
	}

	std::string line;
	std::string source;

	while (getline(file, line))
		source += line + '\n';

	file.close();

	sources_.push_back(source);

}

bool GL::Shader::compile()
{
	bool success = false;

	const char** code = new const char*[sources_.size()];
	for (unsigned int i = 0; i < sources_.size(); i++)
		code[i] = sources_[i].c_str();

	glShaderSource(id_, sources_.size(), code, nullptr);
	glCompileShader(id_);

	int compile = false;

	glGetShaderiv(id_, GL_COMPILE_STATUS, &compile);
	success = (compile == GL_TRUE);

	if (!success)
	{
		GLint length = 0;
		glGetShaderiv(id_, GL_INFO_LOG_LENGTH, &length);
		std::vector<GLchar> infoLog(length);
		glGetShaderInfoLog(id_, length, &length, &infoLog[0]);
#ifdef _DEBUG
		std::cout << "[Shader output] Error :" << &infoLog[0] << std::endl;
#endif 
	}

	delete[] code;

	return success;
}

GL::Shader::~Shader(){}