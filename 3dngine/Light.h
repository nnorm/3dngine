#pragma once
#include "Entity.h"
#include "Common.h"

namespace Render
{
	class Light : public Entity
	{
	public:
		enum Type : int { POINT = 1, SPOT, SUN };
		Light(const std::string &, const glm::vec4 &, const glm::vec4 &, float, Light::Type) throw (std::logic_error);

		virtual void toString(std::ostream & o) const;

		//accesseurs
		Light::Type getType() const;
		float getIntensity() const;
		glm::vec4 getColor() const;

		//modificateurs
		void setType(Light::Type);
		void setIntensity(float) throw (std::logic_error);
		void setColor(const glm::vec4 &);

		private:
			Light::Type _type;
			float _intensity;
			glm::vec4 _color;
	};
}