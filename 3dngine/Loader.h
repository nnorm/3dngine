#pragma once

#include "Common.h"

namespace Assets{

	class Loader{

		protected:

			static Loader* _instance;
			virtual void cache() = 0;

		public:

			const static int CACHE = 10;
			Loader();
			static Loader* getInstance();
			virtual void clean() = 0;
	};
}

