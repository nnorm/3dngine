#include "Texture.h"

using namespace std;
using namespace Assets;

Texture::Texture(const unsigned char* image, long width, long height)
{
	_width = width; // récupération de la largeur de l'image
	_height = height; // récupération de la hauteur de l'image

	glGenTextures(1, &_textureID);
	glBindTexture(GL_TEXTURE_2D, _textureID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	
	glTexImage2D(GL_TEXTURE_2D,		// target
				 0,					// level, 0 = base, no minimap,
				 GL_RGBA,			// internalformat
				 _width,			// width
				 _height,			// height
				 0,					// border, always 0 in OpenGL ES
				 GL_RGBA,			// format
				 GL_UNSIGNED_BYTE,	// type
				 image);

	glDisable(GL_TEXTURE_2D);
}


Texture::~Texture(){}

const GLuint& Texture::getID() const
{
	return _textureID;
}
