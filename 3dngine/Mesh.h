#pragma once

#include "Common.h"
#include <memory>

namespace GL{ class Buffer; }
namespace Assets{

	class Object;

	class Mesh
	{
		private:

			GL::Buffer* _vbo;
			void createBuffer(const std::vector<float *> &, const std::vector<float *> &, const std::vector<float *> &, const std::vector<int *> &);
			std::string _name;
			int _size;
			std::vector<GLfloat> * _data;

		public:

			Mesh(const Object &);
			Mesh(const Mesh &);
			Mesh & operator=(const Mesh &);
			~Mesh();
			const GL::Buffer & getVBO() const;
			void toString(std::ostream &)const;
	};

	std::ostream & operator <<(std::ostream &, const Mesh &);
}