#include "ObjLoader.h"
#include "Toolbox.h"

using namespace std;

namespace Assets{

	Object::Object(const std::string & name)
			: _name(name), _v(std::vector<float*>()), _vn(std::vector<float*>()), _vt(std::vector<float*>()), _elements(std::vector<int*>())
	{}

	Object::~Object()
	{
		for (float* a : _v)
			delete[] a;
		for (float* a : _vn)
			delete[] a;
		for (float* a : _vt)
			delete[] a;
		for (int* a : _elements)
			delete[] a;
	}

	Object::Object(const Object& o)
	{
		_name = o._name;
		for (int i(0); i < o._v.size(); ++i)
		{
			float* vertex = new float[ObjLoader::VSIZE];
			int j = 0;
			while (j++<ObjLoader::VSIZE)
				vertex[j-1] = o._v[i][j-1];
			_v.push_back(vertex);
		}
		for (int i(0); i < o._vt.size(); ++i)
		{
			float* coord = new float[ObjLoader::TSIZE];
			int j = 0;
			while (j++ < ObjLoader::TSIZE)
				coord[j-1] = o._vt[i][j-1];
			_vt.push_back(coord);
		}
		for (int i(0); i < o._vn.size(); ++i)
		{
			float* normal = new float[ObjLoader::VNSIZE];
			int j = 0;
			while (j++<ObjLoader::VNSIZE)
				normal[j-1] = o._vn[i][j-1];
			_vn.push_back(normal);
		}
		for (int i(0); i < o._elements.size(); ++i)
		{
			int* elements = new int[ObjLoader::ELEMENT_NUMBER*ObjLoader::ELEMENT_SIZE];
			int j = 0;
			while (j++<ObjLoader::ELEMENT_NUMBER*ObjLoader::ELEMENT_SIZE)
				elements[j-1] = o._elements[i][j-1];
			_elements.push_back(elements);
		}
	}


	void Object::toString(ostream & o) const{
		o << "name : " <<_name << "\n";
		o << "vertex : ";
		for (int i(0); i < _v.size(); ++i)
		{
			o << "[";
			int j = 0;
			while (j++ < ObjLoader::VSIZE)
				o << _v[i][j - 1] << ", ";
			o << "] ,";
		}
		o << "\ntexture coord : ";
		for (int i(0); i < _vt.size(); ++i)
		{
			o << "[";
			int j = 0;
			while (j++ < ObjLoader::TSIZE)
				o << _vt[i][j - 1] << ", ";
			o << "] ,";
		}
		o << "\nnormal : ";
		for (int i(0); i < _vn.size(); ++i)
		{
			o << "[";
			int j = 0;
			while (j++ < ObjLoader::VNSIZE)
				o << _vn[i][j - 1] << ", ";
			o << "] ,";
		}
		o << "\nelements : ";
		for (int i(0); i < _elements.size(); ++i)
		{
			o << "[";
			int j = 0;
			while (j++ < ObjLoader::ELEMENT_NUMBER*ObjLoader::ELEMENT_SIZE)
				o << _elements[i][j - 1] << ", ";
			o << "] ,";
		}
	}

	ostream & operator <<(ostream & o, const Object & ob){
		ob.toString(o);
		return o;
	}

	Object& Object::operator=(const Object& o)
	{
		if (this != &o){
			for (float* a : _v)
				delete[] a;
			for (float* a : _vn)
				delete[] a;
			for (float* a : _vt)
				delete[] a;
			for (int* a : _elements)
				delete[] a;
			_name = o._name;
			for (int i(0); i < o._v.size(); ++i)
			{
				float* vertex = new float[ObjLoader::VSIZE];
				int j = 0;
				while (j++<ObjLoader::VSIZE)
					vertex[j - 1] = o._v[i][j - 1];
				_v.push_back(vertex);
			}
			for (int i(0); i < o._vt.size(); ++i)
			{
				float* coord = new float[ObjLoader::TSIZE];
				int j = 0;
				while (j++ < ObjLoader::TSIZE)
					coord[j - 1] = o._vt[i][j - 1];
				_vt.push_back(coord);
			}
			for (int i(0); i < o._vn.size(); ++i)
			{
				float* normal = new float[ObjLoader::VNSIZE];
				int j = 0;
				while (j++<ObjLoader::VNSIZE)
					normal[j - 1] = o._vn[i][j - 1];
				_vn.push_back(normal);
			}
			for (int i(0); i < o._elements.size(); ++i)
			{
				int* elements = new int[ObjLoader::ELEMENT_NUMBER*ObjLoader::ELEMENT_SIZE];
				int j = 0;
				while (j++<ObjLoader::ELEMENT_NUMBER*ObjLoader::ELEMENT_SIZE)
					elements[j - 1] = o._elements[i][j - 1];
				_elements.push_back(elements);
			}
		}
		return *this;
	}

	ObjLoader* ObjLoader::_instance = new ObjLoader();

	ObjLoader::ObjLoader()
		: Loader()
	{}

	ObjLoader::~ObjLoader()
	{
		for (map<string, vector<Mesh*>*>::iterator it = _m.begin(); it != _m.end(); ++it)
		{
			for (int i(0); i < it->second->size(); i++)
				delete (*(it->second))[i];
			delete it->second;
		}
	}

	ObjLoader * ObjLoader::getInstance(){
		return ObjLoader::_instance;
	}

	const vector<Mesh*> & ObjLoader::load(const std::string & str) throw(invalid_argument)
	{
		for (auto const &ent1 : _m)
			if (ent1.first == str)
				return *ent1.second;

		std::vector<Object*> objets;
		ifstream file(str, ios::in);
		if (file)
			try
			{
				string line;
				while (getline(file, line))
				{
					vector<string> data = removeNull(split(line, ' '));
					if (data.size() > 0){
						string dat0 = data[0];

						if (dat0 == "o")
							objets.push_back(new Object(data[1]));

						Object *o = objets.back();

						if (dat0 == "f")
						{
							int* tab = new int[ELEMENT_NUMBER*ELEMENT_SIZE];
							int i = 0;
							while (i++ < ELEMENT_NUMBER){
								vector<string> d = split(data[i], '/');
								int j = 0;
								while (j++ < ELEMENT_SIZE)
									tab[(i - 1)*ELEMENT_SIZE + (j - 1)] = stoi(d[j - 1]);
							}
							o->_elements.push_back(tab);
						}

						else if (dat0 == "vt")
						{
							float* tab = new float[TSIZE];
							int i = 0;
							while (i++<TSIZE)
								tab[i - 1] = stof(data[i]);
							o->_vt.push_back(tab);
						}

						else 
						{
							float* tab = new float[VSIZE];
							int i = 0;
							
							if (dat0 == "v")
							{
								while (i++<VSIZE)
									tab[i-1] = stof(data[i]);
								o->_v.push_back(tab);
							}

							else if (dat0 == "vn")
							{
								while (i++<VNSIZE)
									tab[i - 1] = stof(data[i]);
								o->_vn.push_back(tab);
							}
						}
					}
				}
				file.close();
				vector<Mesh*>* v = new vector<Mesh*>();
				for (unsigned i = 0; i < objets.size(); i++)
					v->push_back(new Mesh(*objets[i]));
				_m.insert(pair<string, vector<Mesh*>*>(str, v));
				for (Object* a : objets)
					delete a;
				objets.clear();
				cache();
				return *v;
			}
			catch (const exception & e)
			{
				string msg = " : Incorect syntax file";
				throw invalid_argument(e.what()+msg);
			}
		else
			throw invalid_argument("File can't be open");
	}

	void ObjLoader::cache(){
		if (_m.size() >= Loader::CACHE){
			map<string, vector<Mesh*>*>::iterator it = _m.begin();
			for (int i(0); i < it->second->size(); i++)
				delete (*(it->second))[i];
			delete it->second;
			_m.erase(_m.begin());
		}
	}

	void ObjLoader::clean(){
		for (map<string, vector<Mesh*>*>::iterator it = _m.begin(); it != _m.end(); ++it)
		{
			for (int i(0); i < it->second->size(); i++)
				delete (*(it->second))[i];
			delete it->second;
		}

		_m.clear();
	}

}