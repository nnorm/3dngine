#pragma once

#include "Common.h"
#include "Loader.h"

#include "Mesh.h"

namespace Assets{

	class Object{

		public:

			std::vector<float*> _v, _vn, _vt;
			std::vector<int*> _elements;
			std::string _name;

			Object(const std::string & name);
			~Object();
			Object(const Object&);
			Object& operator=(const Object&);
			void toString(std::ostream &) const;
	};

	std::ostream & operator <<(std::ostream &, const Object &);

	class ObjLoader : public Loader{

		private:

			static ObjLoader * _instance;
			std::map<std::string,std::vector<Mesh*>*> _m;
			void cache();
			ObjLoader();
			~ObjLoader();
			ObjLoader(const ObjLoader &){}
			ObjLoader& operator=(const ObjLoader &){}

		public:

			const static int VSIZE = 3;
			const static int VNSIZE = 3;
			const static int TSIZE = 2;
			const static int ELEMENT_NUMBER = 3;
			const static int ELEMENT_SIZE = 3;
			virtual const std::vector<Mesh*>& load(const std::string &) throw(std::invalid_argument);
			static ObjLoader* getInstance();
			virtual void clean();
	};
}