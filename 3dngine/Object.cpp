#include "Object.h"
#include "ObjLoader.h"
#include "Material.h"

using namespace std;
using namespace Assets;
namespace Render
{
	Object::Object(const std::string & name, const std::vector<Assets::Mesh*> & mesh,const Material & material, const glm::vec4 & pos, const glm::vec3 & rot) 
		: Entity(name, pos, rot), _mesh(mesh), _material(material)
	{}

	void Object::toString(ostream & o) const
	{
		o << "Object";
		Entity::toString(o);
		o << "\n\t";
		for (Assets::Mesh * mesh : _mesh){
			mesh->toString(o);
			o << "\n\t";
		}
		o << "Material : ";
		o << _material;
	}

	const vector<Assets::Mesh *>& Object::getMeshes() const{
		return _mesh;
	}

	const Material& Object::getMaterial() const{
		return _material;
	}
}

