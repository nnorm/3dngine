#pragma once

#include "Common.h"

namespace Render
{
	class Entity
	{
		protected:
			static int count;
			std::string _name;
			int _id;
			//matrice de transformation par rapport au parent
			glm::mat4 _child2Parent;
			//matrice de transformation par rapport � la racine de la sc�ne
			glm::mat4 _local2Global;
			//racine de l'arbre, �l�ment parent
			Entity * _racine;
			//fils de l'entit� courrante
			std::vector<Entity *> _children;
			//position
			glm::vec3 _rot;
			glm::vec4 _pos;

			static void refresh(Entity*);
		private:
			Entity(const Entity &);
			Entity & operator=(Entity &);

		public:
			enum axes{
				X = 0,
				Y,
				Z
			};

			Entity(const std::string &, const glm::vec4 &, const glm::vec3 &);
			virtual ~Entity();
			virtual void toString(std::ostream & o) const = 0;

			//Accesseur
			std::string getName() const;
			int getId() const;
			glm::mat4 getChild2Parent() const;
			glm::mat4 getLocal2Global() const;
			Entity * getRacine() const;
			const std::vector<Entity *>& getChildren() const;
			virtual glm::vec4 getPos() const;
			virtual glm::vec3 getRot() const;

			//Modificateurs
			void setChild2Parent(const glm::mat4 &);
			void setRacine(Entity *);
			void setChildren(std::vector<Entity *>);
			virtual void setPos(const glm::vec4 &);
			virtual void move(const glm::vec4 &);
			virtual void setRotation(const axes &, float);
			virtual void rotate(const axes &, float);

			//fonctions sur les enfants de l'entit� courrante
			void addChild(Entity *);
			Entity * removeChild(int);
	};

	std::ostream & operator <<(std::ostream &, const Entity &);

}
